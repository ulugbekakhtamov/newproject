from django.urls import path

from .api_views import (CustomerListAPIView,
                        CategoryAPIView,
                        SmartphoneListAPIView,
                        NotebookListAPIView,
                        NotebookDetailApiView,
                        )
urlpatterns = [
    path('customers/', CustomerListAPIView.as_view(),  name='customers_list'),
    path('catigories/<str:id>', CategoryAPIView.as_view(),  name='catigories_list'),
    path('smartphone/', SmartphoneListAPIView.as_view(),  name='smartphone_list'),
    path('notebook/', NotebookListAPIView.as_view(),  name='notebook_list'),
    path('notebook/<str:id>', NotebookDetailApiView.as_view(),  name='notebook')
]