from collections import OrderedDict
from rest_framework.response import Response
from rest_framework.generics import ListAPIView, RetrieveAPIView, ListCreateAPIView, RetrieveUpdateAPIView
from rest_framework.filters import SearchFilter
from rest_framework.pagination import PageNumberPagination

from .serializers import CategorySerializers, SmartphoneSerializers, NotebookSerializers, CustomerSerializers, Orderserializers
from ..models import Category,  Customer, Order


class CustomerListAPIView(ListAPIView):

    serializer_class = CustomerSerializers
    queryset = Customer.objects.all()

class OrderListApiView(ListAPIView):

    serializer_class = Orderserializers
    queryset = Order.objects.all()

class CategoryPagination(PageNumberPagination):

    page_size = 2
    page_query_param = 'page_size'
    max_page_size = 10

    def get_paginated_response(self, data):
        return Response(OrderedDict([
            ('object_count', self.page.paginator.count),
            ('next', self.get_next_link()),
            ('previous', self.get_previous_link()),
            ('items', data)
        ]))


class CategoryAPIView(ListCreateAPIView, RetrieveUpdateAPIView):

    pagination_class = CategoryPagination
    serializer_class = CategorySerializers
    queryset = Category.objects.all()
    lookup_field = 'id'

class SmartphoneListAPIView(ListAPIView):

    serializer_class = SmartphoneSerializers

class NotebookListAPIView(ListAPIView):

    serializer_class = NotebookSerializers
    filter_backends = [SearchFilter]
    search_fields = ['price', 'title']

class NotebookDetailApiView(RetrieveAPIView):

    serializer_class = NotebookSerializers
    lookup_field = 'id'